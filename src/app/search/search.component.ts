import { Component, OnInit } from '@angular/core';
import { QueryResult } from '../models/query-result';
import { QueryService } from '../query.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public queryResult: QueryResult | undefined;
  public showForm = true;
  public showLoader = false;
  public showResults = false;

  constructor(private queryService: QueryService) {
  }

  ngOnInit(): void {
  }

  submit() {
    //this.queryService.getData().subscribe();

    this.showForm = false;
    this.showLoader = true;
    
    const timer = setTimeout(() => {
      this.queryResult = {
        count: 0,
        divindat: 'sospecha',
        score: '',
        status: 'bajo'
      }

      this.showLoader = false;
      this.showResults = true;
    }, 1000)

  }
  
}

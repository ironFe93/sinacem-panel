export interface QueryResult {
  status: string;
  count: number;
  divindat: string;
  score: string;
}
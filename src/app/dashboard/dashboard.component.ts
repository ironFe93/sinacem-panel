import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface Operation {
  date: string;
  username: string;
  ipAddress: string;
  transactionType: string;
  category: string;
  subCategory: string;
  data: string;
  score: string;
}

const ELEMENT_DATA: Operation[] = [
  {
    date: '17/11/2021',
    username: 'diego.rodriguez',
    ipAddress: '192.168.1.10',
    transactionType: 'Consulta',
    category: 'Dispositivo',
    subCategory: 'IMEI',
    data: '010928/00/389023/3',
    score: 'Bajo',
  },
  {
    date: '17/11/2021',
    username: 'diego.rodriguez',
    ipAddress: '192.168.1.10',
    transactionType: 'Consulta',
    category: 'Dispositivo',
    subCategory: 'IMEI',
    data: '010928/00/389023/4',
    score: 'Medio',
  },
  {
    date: '17/11/2021',
    username: 'diego.rodriguez',
    ipAddress: '192.168.1.10',
    transactionType: 'Consulta',
    category: 'Dispositivo',
    subCategory: 'IMEI',
    data: '010928/00/389023/5',
    score: 'Alto',
  },
];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = [
    'date',
    'username',
    'ipAddress',
    'transactionType',
    'category',
    'subCategory',
    'data',
    'score',
  ];
  dataSource = ELEMENT_DATA;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToSearch() {
    this.router.navigate(['/search']);
  }

  goToRegister() {
    this.router.navigate(['/register']);
  }

  goToConfiguration() {
    this.router.navigate(['/configuration']);
  }
}

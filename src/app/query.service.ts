import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { QueryResult } from './models/query-result';

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  private queryUrl = '';

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get<QueryResult[]>(this.queryUrl)
  }
}
